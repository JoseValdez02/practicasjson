// JavaScript para obtener la lista de razas de perros y mostrar imágenes

const razasSelect = document.getElementById('razas');
const imagenPerro = document.getElementById('imagenPerro');
const cargarRazasBtn = document.getElementById('cargarRazas');
const verImagenBtn = document.getElementById('verImagen');

// Obtener lista de razas de perros
function cargarRazas() {
  fetch('https://dog.ceo/api/breeds/list')
    .then(response => response.json())
    .then(data => {
      data.message.forEach(raza => {
        const option = document.createElement('option');
        option.text = raza;
        razasSelect.add(option);
      });
    });
}

// Mostrar imagen de la raza seleccionada
function verImagenPorRaza() {
  const razaSeleccionada = razasSelect.value;
  if (razaSeleccionada) {
    fetch(`https://dog.ceo/api/breed/${razaSeleccionada}/images/random`)
      .then(response => response.json())
      .then(data => {
        imagenPerro.src = data.message;
      });
  }
}

// Mostrar imagen aleatoria de cualquier raza
function verImagenAleatoria() {
  fetch('https://dog.ceo/api/breeds/image/random')
    .then(response => response.json())
    .then(data => {
      imagenPerro.src = data.message;
    });
}

cargarRazasBtn.addEventListener('click', cargarRazas);
verImagenBtn.addEventListener('click', verImagenAleatoria);
razasSelect.addEventListener('change', verImagenPorRaza);
