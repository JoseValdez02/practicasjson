
async function buscarPais() {
    const nombrePaisInput = document.getElementById('nombrePaisInput').value.trim();

    if (nombrePaisInput === '') {
        alert('Por favor, llena el apartado del nombre del país antes de buscar.');
        return;
    }

    const url = `https://restcountries.com/v3.1/name/${nombrePaisInput}`;

    try {
        const response = await fetch(url);
        const countryData = await response.json();

        if (countryData.length > 0) {
            const capital = countryData[0].capital || 'No disponible';
            
            document.getElementById('capitalResult').innerText = `Capital: ${capital}`;

            const languagesObj = countryData[0].languages;
            const languagesText = languagesObj ? Object.values(languagesObj).join(', ') : 'No disponible';
            document.getElementById('lenguajeResult').innerText = `Idiomas: ${languagesText}`;
        } else {
            alert('No se encontró el país especificado');
            limpiar();
        }
    } catch (error) {
        console.error('Error al realizar la petición:', error);
        alert('Error al obtener datos del país');
    }
}

function limpiar() {
    document.getElementById('capitalResult').innerText = 'Capital:';
    document.getElementById('lenguajeResult').innerText = 'Idiomas:';
    document.getElementById('nombrePaisInput').value = "";
}
