
function getUserById() {
    var userId = document.getElementById("userIdInput").value;

    if (!userId) {
        alert("Ingrese un ID válido");
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://jsonplaceholder.typicode.com/users/" + userId, true);

    xhr.onload = function () {
        if (xhr.status === 200) {
            var user = JSON.parse(xhr.responseText);
            displayUser(user);
        } else {
            console.error('Error al obtener usuario. Código de estado:', xhr.status);
            clearResultContainer();
        }
    };

    xhr.send();
}

function displayUsers(users) {
    var resultContainer = document.getElementById("resultContainer");
    resultContainer.innerHTML = "";

    users.forEach(function (user) {
        var userDiv = document.createElement("div");
        userDiv.textContent = "ID: " + user.id + ", Nombre: " + user.name + ", Email: " + user.email;
        resultContainer.appendChild(userDiv);
    });
}

function displayUser(user) {
    var resultContainer = document.getElementById("resultContainer");
    resultContainer.innerHTML = "";

    var userDiv = document.createElement("div");
    userDiv.textContent = "ID: " + user.id + ", Nombre: " + user.name + ", Email: " + user.email;
    resultContainer.appendChild(userDiv);
}

function clearResultContainer() {
    var resultContainer = document.getElementById("resultContainer");
    resultContainer.innerHTML = "";
}
