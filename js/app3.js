function cargarDatos(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users"

    //realizar funcion de respuesta de peticion
    http.onreadystatechange = function(){

        //validar respuesta

        if(this.status== 200 && this.readyState==4){

            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            //ciclo para mostrar los datos en la tabla

            for(const datos of json){
                res.innerHTML+= '<tr> <td class="columna1">' + datos.id + '</td>'
                + '<td class="columna2">' + datos.name + '</td>'
                + '<td class="columna3">' + datos.username + '</td>'
                + '<td class="columna4">' + datos.email + '</td> </tr>'

            }
        } //else alert("surgio un error al hacer la peticion")

    }

    http.open('GET',url,true);
    http.send();

}

//codificar los botones
document.getElementById("btnCargar").addEventListener('click', cargarDatos());
document.getElementById("btnCargar").addEventListener('click',cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click",function(){
    let res = document.getElementById('lista');
    res.innerHTML="";

})