
function getAlbumById() {
    var userId = document.getElementById("userIdInput").value;

    if (!userId) {
        alert("Ingrese un ID válido");
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://jsonplaceholder.typicode.com/albums?userId=" + userId, true);

    xhr.onload = function () {
        if (xhr.status === 200) {
            var albums = JSON.parse(xhr.responseText);
            displayAlbums(albums);
        } else {
            console.error('Error al obtener álbumes. Código de estado:', xhr.status);
            clearResultContainer();
        }
    };

    xhr.send();
}

function displayAlbums(albums) {
    var resultContainer = document.getElementById("resultContainer");
    resultContainer.innerHTML = "";

    albums.forEach(function (album) {
        var userDiv = document.createElement("div");
        userDiv.textContent = "userId: " + album.userId + ", ID: " + album.id + ", Titulo: " + album.title;
        resultContainer.appendChild(userDiv);
    });
}

function displayAlbum(album) {
    var resultContainer = document.getElementById("resultContainer");
    resultContainer.innerHTML = "";

    var userDiv = document.createElement("div");
    userDiv.textContent = "userID: " + album.userId + ", ID: " + album.id + ", Titulo: " + album.title;
    resultContainer.appendChild(userDiv);
}

function clearResultContainer() {
    var resultContainer = document.getElementById("resultContainer");
    resultContainer.innerHTML = "";
}